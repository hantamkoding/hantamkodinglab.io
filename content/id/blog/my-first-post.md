---
title: "Kesan pertama degan HUGO"
date: 2021-06-10T15:08:48+07:00
lastmod: 2020-03-05T15:58:26+08:00
tags: ["content", "Blog", "Pengalaman"]
categories: ["Blog"]
image: images/blog/my-first-post.jpg
author: "Hantamkoding"
description: "This is meta description"
---

Halo perkenalkan ini adalah ketikan pertamaku menggunakan framework hugo

yang entah kenapa saya tertarik untuk sedikit menyimpan beberapa tulisan di blog setelah sekian lama, cari mencari awalnya saya tertarik dengan blog builder lain seperti wordpress
dan blogspot

mau bagaimana lagi itulah blog builder yang pertama saya kenal, mungkin saya tidak banyak bisa bermain kata kata dalam mengetik artikel tapi mulai hari ini saya ingin mengungkapkan
apa yang saya pelajari dan beberapa pengalaman saya di dunia internet 😁

## Kenapa saya memilih menggunakan hugo ??

Dipikiranku aku butuh sebuah wadah yang saya butuh kan untuk mengetik dan mudah dalam mempublish tanpa biaya internet yang berlebih untuk sekedar menulis dan berbagi artikel 🤔
sampai akhirnya saya teringat tentang **static site hosting** dan tentunya gratis 😁

Alhasil saya mencoba melakukan pencarian dan menemukan laman https://www.techradar.com/best/static-site-generators

Seperti halnya cinta pada pandangan pertama mata saya langsung tertuju pada **HUGO** yah karena alasan spele yaitu simpe dan mudah di gunakan karena penulisan langsung di komputer
dan bisa menggunakan markdown alhasil syaa mantapkan dan menulis artikel pengenalan ini hehe,

Mungkin sekian ya untuk pengenalan kali ini, saya masih penasaran akan mengesklor tentang framework ini

#### Ditulis dengan :

- OS: Linux Manjaro
- Editor : NeoVim 0.4
